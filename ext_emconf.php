<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "bodyclass".
 *
 * Auto generated 13-05-2016 09:46
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Body class',
  'description' => 'Set individual class and / or id attribute for body tag. Automatically inserts page id, parent id or lang id in body tag.',
  'category' => 'fe',
  'version' => '2.1.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearcacheonload' => false,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => '',
  'constraints' =>
  array (
    'depends' =>
    array (
      'typo3' => '7.6.0-7.6.99',
    ),
    'conflicts' =>
    array (
    ),
    'suggests' =>
    array (
    ),
  ),
);
<?php
namespace SBTheke\Bodyclass\UserFunc;

class BodyClass {

    public static function user_randomClasses() {
        $randomStyles = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $GLOBALS['TSFE']->tmpl->setup['plugin.']['bodyclass.']['randomClasses']);
        // Get random class
        $randomBodyClass = $randomStyles[array_rand($randomStyles)];
        if($GLOBALS['TSFE']->tmpl->setup['plugin.']['bodyclass.']['storeRandomClassInCookie']) {
            if(!$GLOBALS['TSFE']->fe_user->getKey('ses', 'bodyClassStyle')) {
                // Store random class in session
                $GLOBALS['TSFE']->fe_user->setKey('ses', 'bodyClassStyle', $randomBodyClass);
            }
            return $GLOBALS['TSFE']->fe_user->getKey('ses', 'bodyClassStyle');
        } else {
            return $randomBodyClass;
        }
    }

}
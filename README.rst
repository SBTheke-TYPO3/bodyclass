=============
Documentation
=============

----------------
What does it do?
----------------

This extension allows the editor to set an individual class and / or id attribute for the body tag. If configured, page id, parent page id, language id or random classes are automatically inserted in the body tag.

The class and / or id attribute for the body tag is set automatically or individually by an editor in the page properties.
The automatically id attribute consists of the string "page_" and the page alias or page-id, if there's no alias.
The automatically class attribute consists of the string "parent_" and the alias or page-id of the parent page.
You can change the prepended string via TypoScript.
You can define random class names, which are optional saved in the session.


-----------
Screenshots
-----------

.. figure:: Documentation/screenshot1.png

    You can activate and define the id and class attribute in page properties

.. figure:: Documentation/screenshot2.png

    Easy and comprehensive configuration in the Constant Editor


-------------
Configuration
-------------

After installing this extension via the extension manager and updating the database, you have to include the extension template in field "Include static (from extensions)" in your main template.


Overview of TypoScript constants
================================

====================================  =========  ====================  ===========
Property                              Data type  Default               Description
====================================  =========  ====================  ===========
enableBodyId                          boolean    1                     Enable ID: Body tag gets ID attribute: Value from alias field or page ID.
enableBodyClass                       boolean    1                     Enable class: Body tag gets class attribute: You can type in the value or the page ID from parent page is taken (e.g. "parent_123").
enableGeneratedClass                  boolean    1                     Enable generated class: If deactivated, the body tag gets no class attribute generated from parent ID (see setting "enableBodyClass"); only the value you typed in in page properties is taken.
enableLanguageClass                   boolean    1                     Enable language class: Body tag gets class attribute generated from current language ID (e.g. "lang_2").
enableRandomClass                     boolean    0                     Enable random class: Body tag gets a random class. The class changes every page request.
randomClasses                         string     style1,style2,style3  Define the random classes: Type in the pool for random classes, serarated by comma.
storeRandomClassInCookie              boolean    0                     Save random class: The random class (which was chosen on the first page request) is saved in session, so the random body class only changes when the user restarts his browser.
overwriteGeneratedByIndividualClass   boolean    0                     Overwrite generated class: The automatically generated class (see setting "enableGeneratedClass") is removed, if you type in a class in page properties.
enableOnlyOnSelectedPages             boolean    0                     Enable ID and class only on selected pages: Body tag gets ID and class attribute only, if you activate a checkbox in page properties. The body tag on all other pages remains untouched.
====================================  =========  ====================  ===========


---
FAQ
---

Usage with TYPO3 < 7
====================

Please use version 2.0.0 from TER.


Usage with TYPO3 < 6.2
======================

Please use version 1.4.1 from TER.


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.
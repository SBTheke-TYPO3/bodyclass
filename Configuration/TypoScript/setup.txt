// Id and class attribute for body tag
plugin.bodyclass = USER
plugin.bodyclass {
    enableBodyId = {$plugin.bodyclass.enableBodyId}
    enableBodyClass = {$plugin.bodyclass.enableBodyClass}
    enableGeneratedClass = {$plugin.bodyclass.enableGeneratedClass}
    enableRandomClass = {$plugin.bodyclass.enableRandomClass}
    enableLanguageClass = {$plugin.bodyclass.enableLanguageClass}
    randomClasses = {$plugin.bodyclass.randomClasses}
    storeRandomClassInCookie = {$plugin.bodyclass.storeRandomClassInCookie}
    overwriteGeneratedByIndividualClass = {$plugin.bodyclass.overwriteGeneratedByIndividualClass}
    enableOnlyOnSelectedPages = {$plugin.bodyclass.enableOnlyOnSelectedPages}
}

temp.body >
temp.body = COA
temp.body.wrap = <body|>

// Id attribute (alias or uid)
[globalVar = LIT:{$plugin.bodyclass.enableBodyId} = 1]
temp.body.10 = COA
temp.body.10 {
    stdWrap.noTrimWrap = | id="|"|
    10 = CASE
    10 {
        key.field = tx_bodyclass_wrap
        1 = COA
        1 {
            10 = TEXT
            10.if.isTrue.field = tx_bodyclass_wrap_id
            10.dataWrap = {field:tx_bodyclass_wrap_id}
            20 < .10
            20.if.negate = 1
            20.dataWrap = page_{field:alias//field:uid}
        }
        0 = TEXT
        0.dataWrap < .1.20.dataWrap
    }
}
[end]

temp.classes = COA

// Generated and/or individual class attribute (parent alias or uid)
[globalVar = LIT:{$plugin.bodyclass.enableBodyClass} = 1]
temp.classes {
    20 = CASE
    20 {
        key.field = tx_bodyclass_wrap
        1 = COA
        1 {
            10 = TEXT
            10.if.isTrue.field = tx_bodyclass_wrap_class
            10.dataWrap = {field:tx_bodyclass_wrap_class}
            15 = TEXT
            15.noTrimWrap = | |
            20 = TEXT
            20.dataWrap = parent_{levelfield:-2,alias//levelfield:-2,uid}
        }
        0 = TEXT
        0.dataWrap < .1.20.dataWrap
    }
}
[end]

// Overwrite generated class by user typed class
[globalVar = LIT:{$plugin.bodyclass.overwriteGeneratedByIndividualClass} = 1]
temp.classes.20.1 {
    15 >
    20 < .10
    20.if.negate = 1
}
[end]

// No generated class allowed
[globalVar = LIT:{$plugin.bodyclass.enableGeneratedClass} < 1]
temp.classes.20.0 >
temp.classes.20.1.15 >
temp.classes.20.1.20 >
[end]

// Random class attribute
[globalVar = LIT:{$plugin.bodyclass.enableRandomClass} = 1]
temp.classes.29 = TEXT
temp.classes.29.noTrimWrap = | ||
temp.classes.30 = USER_INT
temp.classes.30.userFunc = SBTheke\Bodyclass\UserFunc\BodyClass->user_randomClasses
[end]

// Language class attribute
[globalVar = LIT:{$plugin.bodyclass.enableLanguageClass} = 1]
temp.classes.39 = TEXT
temp.classes.39.noTrimWrap = | ||
temp.classes.40 = TEXT
temp.classes.40.dataWrap = lang_{TSFE:sys_language_uid}
[end]

// Class attribute only if necessary
[globalVar = LIT:{$plugin.bodyclass.enableBodyClass} = 1]
// remove whitespace
temp.classes.stdWrap.trim = 1
temp.body.20 < temp.classes
temp.body.20.stdWrap.noTrimWrap = | class="|"|
temp.body.20.stdWrap.required = 1
[end]

// Enable id and class attribute only on selected pages (checkbox is activated)
[globalVar = LIT:{$plugin.bodyclass.enableOnlyOnSelectedPages} = 1]
temp.body.10.if.isTrue.field = tx_bodyclass_wrap
temp.body.20.if.isTrue.field = tx_bodyclass_wrap
[end]

page.bodyTagCObject >
page.bodyTagCObject < temp.body
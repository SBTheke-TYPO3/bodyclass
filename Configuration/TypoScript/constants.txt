plugin.bodyclass {
    # cat=plugin.bodyclass//10; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:enableBodyId
    enableBodyId = 1
    # cat=plugin.bodyclass//20; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:enableBodyClass
    enableBodyClass = 1
    # cat=plugin.bodyclass//30; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:enableGeneratedClass
    enableGeneratedClass = 1
    # cat=plugin.bodyclass//40; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:enableLanguageClass
    enableLanguageClass = 1
    # cat=plugin.bodyclass//50; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:enableRandomClass
    enableRandomClass = 0
    # cat=plugin.bodyclass//60; type=string; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:randomClasses
    randomClasses = style1,style2,style3
    # cat=plugin.bodyclass//70; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:storeRandomClassInCookie
    storeRandomClassInCookie = 0
    # cat=plugin.bodyclass//80; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:overwriteGeneratedByIndividualClass
    overwriteGeneratedByIndividualClass = 0
    # cat=plugin.bodyclass//90; type=boolean; label=LLL:EXT:bodyclass/Resources/Private/Language/locallang_ts.xlf:enableOnlyOnSelectedPages
    enableOnlyOnSelectedPages = 0
}
<?php
defined('TYPO3_MODE') || die();

// Extension manager configuration
$confArray = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['bodyclass']);

// Add fields for individual id / class for body
if($confArray['individualBodyAttributes'] === NULL || (int)$confArray['individualBodyAttributes'] !== 0) {
    $newPagesColumns = [
        'tx_bodyclass_wrap' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:bodyclass/Resources/Private/Language/locallang_tca.xlf:pages.bodyclass_wrap',
            'requestUpdate' => 1,
            'config' => [
                'type' => 'check',
                'items' => [
                    ['LLL:EXT:lang/locallang_core.xlf:labels.enabled', '']
                ]
            ]
        ],
        'tx_bodyclass_wrap_id' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:bodyclass/Resources/Private/Language/locallang_tca.xlf:pages.bodyclass_wrap_id',
            'displayCond' => 'FIELD:tx_bodyclass_wrap:REQ:true',
            'config' => [
                'type' => 'input',
                'size' => 20,
            ]
        ],
        'tx_bodyclass_wrap_class' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:bodyclass/Resources/Private/Language/locallang_tca.xlf:pages.bodyclass_wrap_class',
            'displayCond' => 'FIELD:tx_bodyclass_wrap:REQ:true',
            'config' => [
                'type' => 'input',
                'size' => 20,
            ]
        ]
    ];
    // Adding fields to the pages table definition in TCA
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $newPagesColumns);

    // Create palette
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', '--palette--;LLL:EXT:bodyclass/Resources/Private/Language/locallang_tca.xlf:pages.palette.bodyclass;bodyclass', '', 'after:subtitle');
    $GLOBALS['TCA']['pages']['palettes']['bodyclass']['showitem'] = 'tx_bodyclass_wrap, tx_bodyclass_wrap_id,tx_bodyclass_wrap_class';

    // Request an update
    $GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] .= (empty($GLOBALS['TCA']['pages']['ctrl']['requestUpdate']) === TRUE ? '' : ',') . 'tx_bodyclass_wrap';
}